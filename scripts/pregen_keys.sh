#!/bin/sh

set -e

if [ ! -d salt/keys ]; then
    mkdir -p salt/keys
fi
ls -1 salt/grains | while read a; do
    if [ ! -f salt/keys/$a.pem ]; then
        openssl genrsa -out salt/keys/$a.pem 2048
    fi
    if [ ! -f salt/keys/$a.pub ]; then
        openssl rsa -in salt/keys/$a.pem -pubout -out salt/keys/$a.pub
    fi
done
