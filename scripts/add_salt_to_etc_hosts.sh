#!/bin/sh

set -e

ip_address="172.16.4.10"
host_name="salt.gva.dev"

matches_in_host="$(grep -n $host_name /etc/hosts | cut -f1 -d:)"
host_entry="${ip_address} ${host_name} salt"

if [ ! -z "$matches_in_host" ]
then
    echo "Updating existing hosts entry"

    echo $matches_in_host | while read -r line_number; do
        sed -i "${line_number}s/.*/${host_entry}/" /etc/hosts
    done
else
    echo "Adding new hosts entry"

    echo "${host_entry}" >> /etc/hosts 
fi
