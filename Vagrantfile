# -*- mode: ruby -*-
# vi: set ft=ruby :

system("scripts/pregen_keys.sh")

Vagrant.configure("2") do |config|
  config.vm.box = "generic/debian10"
  config.vm.post_up_message = nil
  config.vm.synced_folder ".", "/vagrant", disabled: true, type: "9p"

  config.vm.provision "shell",
      path: "scripts/add_salt_to_etc_hosts.sh"

  config.vm.define "salt" do |node|
      node.vm.hostname = "salt"
      node.vm.network "private_network", ip: "172.16.4.10"

      node.vm.provider :libvirt do |libvirt|
          libvirt.cpus = 2
          libvirt.memory = 2048
          libvirt.memorybacking :access, :mode => "shared"
      end

      node.vm.synced_folder "../gvasalt/states", "/srv/salt", type: "9p"
      node.vm.synced_folder "../gvasalt/pillar", "/srv/pillar", type: "9p"

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/salt"
          salt.install_master = true
          salt.masterless = false
          salt.minion_id = "salt"
          salt.run_highstate = false
          salt.minion_key = "salt/keys/salt.pem"
          salt.minion_pub = "salt/keys/salt.pub"
          salt.seed_master = {
            dns: "salt/keys/dns.pub",
            file: "salt/keys/file.pub",
            ldap: "salt/keys/ldap.pub",
            mail: "salt/keys/mail.pub",
            mq: "salt/keys/mq.pub",
            mysql: "salt/keys/mysql.pub",
            pgsql: "salt/keys/pgsql.pub",
            salt: "salt/keys/salt.pub",
            service: "salt/keys/service.pub",
            syslog: "salt/keys/syslog.pub",
            web: "salt/keys/web.pub",
          }
      end
  end

  config.vm.define "mq" do |node|
      node.vm.hostname = "mq"
      node.vm.network "private_network", ip: "172.16.4.20"
      node.vm.network "forwarded_port", guest: 15672, host:8672
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/mq"
          salt.masterless = false
          salt.minion_id = "mq"
          salt.minion_key = "salt/keys/mq.pem"
          salt.minion_pub = "salt/keys/mq.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "syslog" do |node|
      node.vm.hostname = "syslog"
      node.vm.network "private_network", ip: "172.16.4.30"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/syslog"
          salt.masterless = false
          salt.minion_id = "syslog"
          salt.minion_key = "salt/keys/syslog.pem"
          salt.minion_pub = "salt/keys/syslog.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "pgsql" do |node|
      node.vm.hostname = "pgsql"
      #node.vm.synced_folder "repos/gvapgsql", "/srv/gvapgsql"
      node.vm.network "private_network", ip: "172.16.4.40"
      node.vm.provider :libvirt do |libvirt|
        libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/pgsql"
          salt.masterless = false
          salt.minion_id = "pgsql"
          salt.minion_key = "salt/keys/pgsql.pem"
          salt.minion_pub = "salt/keys/pgsql.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "dns" do |node|
      node.vm.hostname = "dns"
      node.vm.network "private_network", ip: "172.16.4.50"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/dns"
          salt.masterless = false
          salt.minion_id = "dns"
          salt.minion_key = "salt/keys/dns.pem"
          salt.minion_pub = "salt/keys/dns.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "ldap" do |node|
      node.vm.hostname = "ldap"
      #node.vm.synced_folder "repos/gvaldap", "/srv/gvaldap"
      node.vm.network "private_network", ip: "172.16.4.60"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/ldap"
          salt.masterless = false
          salt.minion_id = "ldap"
          salt.minion_key = "salt/keys/ldap.pem"
          salt.minion_pub = "salt/keys/ldap.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "file" do |node|
      node.vm.hostname = "file"
      #node.vm.synced_folder "repos/gvafile", "/srv/gvafile"
      node.vm.network "private_network", ip: "172.16.4.70"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/file"
          salt.masterless = false
          salt.minion_id = "file"
          salt.minion_key = "salt/keys/file.pem"
          salt.minion_pub = "salt/keys/file.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "mail" do |node|
      node.vm.hostname = "mail"
      node.vm.network "private_network", ip: "172.16.4.80"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/mail"
          salt.masterless = false
          salt.minion_id = "mail"
          salt.minion_key = "salt/keys/mail.pem"
          salt.minion_pub = "salt/keys/mail.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "mysql" do |node|
      node.vm.hostname = "mysql"
      #node.vm.synced_folder "repos/gvamysql", "/srv/gvamysql"
      node.vm.network "private_network", ip: "172.16.4.90"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/mysql"
          salt.masterless = false
          salt.minion_id = "mysql"
          salt.minion_key = "salt/keys/mysql.pem"
          salt.minion_pub = "salt/keys/mysql.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "web" do |node|
      node.vm.hostname = "web"
      #node.vm.synced_folder "repos/gvaweb", "/srv/gvaweb"
      node.vm.network "private_network", ip: "172.16.4.100"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/web"
          salt.masterless = false
          salt.minion_id = "web"
          salt.minion_key = "salt/keys/web.pem"
          salt.minion_pub = "salt/keys/web.pub"
          salt.run_highstate = false
      end
  end

  config.vm.define "service" do |node|
      node.vm.hostname = "service"
      #node.vm.synced_folder "repos/gva", "/srv/gva"
      node.vm.network "forwarded_port", guest: 443, host:8443
      node.vm.network "private_network", ip: "172.16.4.110"
      node.vm.post_up_message = "Use https://localhost:8443/ to access the gva web interface"
      node.vm.provider :libvirt do |libvirt|
          libvirt.memory = 1024
      end

      node.vm.provision :salt do |salt|
          salt.bootstrap_options = "-x python3"
          salt.grains_config = "salt/grains/service"
          salt.masterless = false
          salt.minion_id = "service"
          salt.minion_key = "salt/keys/service.pem"
          salt.minion_pub = "salt/keys/service.pub"
          salt.run_highstate = false
      end
  end
end
