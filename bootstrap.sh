#!/bin/sh

set -e

origdir=$(dirname $(readlink -f $0))

mkdir -p repos
for repo in gva gvafile gvaldap gvamysql gvapgsql gvasalt gvaweb; do
    if [ ! -d "$origdir/repos/$repo" ]; then
        git clone "https://git.dittberner.info/gnuviech/$repo.git" "$origdir/repos/$repo"
    else
        cd "$origdir/repos/$repo"
        git fetch --all
        cd "$origdir"
    fi
done
vagrant up
